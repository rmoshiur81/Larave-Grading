<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/frm', function () {
    return view('grading.create');
});
Route::post('/process', function(Request $request) {
    $name = $request->name;
    $mark = $request->mark;
    // var_dump($_POST);
    $value='';
    if (strlen($name)>0 && strlen($mark)>0) {
      if (is_numeric($mark)) {
        if ($mark >=0 && $mark <=39) {
          $grade="F";
          $value="Name: $name and Grade: $grade";
        }
        if ($mark >39 && $mark <60) {
          $grade="D";
          $value="Name: $name and Grade: $grade";
        }

        if ($mark >59 && $mark <70) {
          $grade="C";
          $value="Name: $name and Grade: $grade";
        }

        if ($mark >69 && $mark <80) {
          $grade="B";
          $value="Name: $name and Grade: $grade";
        }
        if ($mark >79 && $mark <=100) {
          $grade="A";
          $value="Name: $name and Grade: $grade";
        }
        if ($mark <0 || $mark>100) {
          $value= "Invalid Value You have inserted";
        }
      }else {
        $value="Please enter name and score correctly";
      }


    }else {
      $value= "All fields should be filled up";
    }
    echo $value;
    return view('grading.result');
});
